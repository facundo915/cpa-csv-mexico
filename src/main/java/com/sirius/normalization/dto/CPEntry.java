package com.sirius.normalization.dto;

public class CPEntry {
    private Integer claveAsentamiento;
    private String nombreAsentamiento;
    private String tipoAsentamiento;
    private String nombreMunicipio;
    private String nombreEstado;
    private String nombreCiudad;
    private Integer d_CP;
    private Integer claveEstado;
    private Integer c_oficina;
    private Integer c_CP;
    private Integer claveTipoAsentamiento;
    private Integer claveMunicipio;
    private Integer identificadorUnicoAsentamiento;
    private String zona;
    private Integer claveCiudad;

    public CPEntry(String s) {
        String[] split = s.split("\\|");
        claveAsentamiento = intValue(split[0]);
        nombreAsentamiento = split[1];
        tipoAsentamiento = split[2];
        nombreMunicipio = split[3];
        nombreEstado = split[4];
        nombreCiudad = split[5];
        d_CP = intValue(split[6]);
        claveEstado = intValue(split[7]);
        c_oficina = intValue(split[8]);
        c_CP = intValue(split[9]);
        claveTipoAsentamiento = intValue(split[10]);
        claveMunicipio = intValue(split[11]);
        identificadorUnicoAsentamiento = intValue(split[12]);
        zona = split[13];
        if(split.length > 14)
            claveCiudad = intValue(split[14]);

    }

    private Integer intValue(String s) {
        if(s == null || s.equals(""))
            return null;
        return Integer.valueOf(s);
    }

    public Integer getClaveAsentamiento() {
        return claveAsentamiento;
    }

    public String getNombreAsentamiento() {
        return nombreAsentamiento;
    }

    public String getTipoAsentamiento() {
        return tipoAsentamiento;
    }

    public String getNombreMunicipio() {
        return nombreMunicipio;
    }

    public String getNombreEstado() {
        return nombreEstado;
    }

    public String getNombreCiudad() {
        return nombreCiudad;
    }

    public Integer getD_CP() {
        return d_CP;
    }

    public Integer getClaveEstado() {
        return claveEstado;
    }

    public Integer getC_oficina() {
        return c_oficina;
    }

    public Integer getC_CP() {
        return c_CP;
    }

    public Integer getClaveTipoAsentamiento() {
        return claveTipoAsentamiento;
    }

    public Integer getIdentificadorUnicoAsentamiento() {
        return identificadorUnicoAsentamiento;
    }

    public String getZona() {
        return zona;
    }

    public Integer getClaveCiudad() {
        return claveCiudad;
    }

    public Integer getClaveMunicipio() {
        return claveMunicipio;
    }
}
