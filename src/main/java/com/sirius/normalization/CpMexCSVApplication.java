package com.sirius.normalization;

import com.sirius.normalization.Service.CpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

@SpringBootApplication
public class CpMexCSVApplication implements CommandLineRunner {

    @Autowired
    CpService cpService;

    public static void main(String[] args) {
        SpringApplication.run(CpMexCSVApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Stream<String> lines = Files.lines(Paths.get("/home/facundo/projects/work/active/iot/sepomex/src/main/resources/CPdescarga.txt"), Charset.forName("ISO_8859_1"));
        lines.forEach(cpService::save);
    }
}
