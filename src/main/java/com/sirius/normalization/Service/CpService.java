package com.sirius.normalization.Service;

import com.sirius.normalization.dto.CPEntry;
import com.sirius.normalization.model.*;
import com.sirius.normalization.repository.*;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.Normalizer;
import java.util.Optional;

@Service
public class CpService {
    @Autowired
    AsentamientoRepository asentamientoRepository;
    @Autowired
    CiudadRepository ciudadRepository;
    @Autowired
    EstadoRepository estadoRepository;
    @Autowired
    MunicipioRepository municipioRepository;
    @Autowired
    TipoAsentamientoRepository tipoAsentamientoRepository;
    private int counter = 0;

    @Transactional
    public void save(String s) {
        CPEntry entry = new CPEntry(s);
        /* L E N T O
        Optional<Asentamiento> asentamiento = asentamientoRepository.findAsentamientoByCodigoAndNombre(entry.getClaveAsentamiento(), entry.getNombreAsentamiento());
        if (asentamiento.isPresent()) {
            LoggerFactory.getLogger(CpService.class).error("El asentamiento " + entry.getClaveAsentamiento() + " con nombre " + entry.getNombreAsentamiento() +" ya estaba registrado");
            return;
        }
         */
        Optional<Estado> estado = estadoRepository.findById(entry.getClaveEstado());
        Optional<Municipio> municipio = municipioRepository.findByNombreAndCodigo(entry.getNombreMunicipio(),entry.getClaveMunicipio()); //Warning: los codigos de municipio no son unicos (ex: Rioverde = Parras = 24)
        Optional<Ciudad> ciudad = entry.getClaveCiudad() == null ? Optional.empty() : ciudadRepository.findById(entry.getClaveCiudad());
        Optional<TipoAsentamiento> tipoAsentamiento = tipoAsentamientoRepository.findById(entry.getClaveTipoAsentamiento());

        if (!tipoAsentamiento.isPresent()) {
            TipoAsentamiento tipo = new TipoAsentamiento(entry.getClaveTipoAsentamiento(), entry.getTipoAsentamiento());
            tipoAsentamiento = Optional.of(tipoAsentamientoRepository.save(tipo));
        }

        if (!ciudad.isPresent() && entry.getClaveCiudad() != null) {
            Ciudad c = new Ciudad(entry.getClaveCiudad(), entry.getNombreCiudad(),normalize(normalize(entry.getNombreCiudad())));
            ciudad = Optional.of(ciudadRepository.save(c));
        }

        if (!estado.isPresent()) {
            Estado es = new Estado(entry.getClaveEstado(), entry.getNombreEstado(),normalize(entry.getNombreEstado()));
            estado = Optional.of(estadoRepository.save(es));
        }
        if (!municipio.isPresent()) {
            Municipio m = new Municipio(entry.getClaveMunicipio(), entry.getNombreMunicipio(), normalize(entry.getNombreMunicipio()), estado.get());
            municipio = Optional.of(municipioRepository.save(m));
        }

        Asentamiento as = new Asentamiento(entry.getClaveAsentamiento(), entry.getNombreAsentamiento(), normalize(entry.getNombreAsentamiento()),
                tipoAsentamiento.get(), entry.getIdentificadorUnicoAsentamiento(), municipio.get());
        ciudad.ifPresent(as::setCiudad);
        asentamientoRepository.save(as);
        LoggerFactory.getLogger(CpService.class).info("Count: " + ++counter);

    }

    //Remove tildes and lowerCase the string
    private String normalize(String s) {
        return Normalizer
                .normalize(s, Normalizer.Form.NFD)
                .replaceAll("[^\\p{ASCII}]", "")
                .toLowerCase()
                .trim();

    }
}
