package com.sirius.normalization.repository;

import com.sirius.normalization.model.TipoAsentamiento;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TipoAsentamientoRepository extends JpaRepository<TipoAsentamiento,Integer> {
}
