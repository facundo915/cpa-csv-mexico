package com.sirius.normalization.repository;

import com.sirius.normalization.model.Estado;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EstadoRepository extends JpaRepository<Estado,Integer> {
}
