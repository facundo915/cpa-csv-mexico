package com.sirius.normalization.repository;

import com.sirius.normalization.model.Municipio;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MunicipioRepository extends JpaRepository<Municipio,Integer> {
    Optional<Municipio> findByNombreAndCodigo(String nombre, Integer codigo);
}
