package com.sirius.normalization.repository;

import com.sirius.normalization.model.Asentamiento;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AsentamientoRepository extends JpaRepository<Asentamiento,Integer> {
    Optional<Asentamiento> findAsentamientoByCodigoAndNombre(Integer codigo,String nombre);
}
