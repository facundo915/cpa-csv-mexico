package com.sirius.normalization.repository;

import com.sirius.normalization.model.Ciudad;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CiudadRepository extends JpaRepository<Ciudad,Integer> {
}
