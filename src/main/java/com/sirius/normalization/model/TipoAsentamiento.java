package com.sirius.normalization.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class TipoAsentamiento {
    @Id
    private Integer codigo;
    private String nombre;
    @OneToMany(
            mappedBy = "tipoAsentamiento",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    private Set<Asentamiento> asentamientos = new HashSet<>();

    public TipoAsentamiento() {
    }

    public TipoAsentamiento(Integer codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (!(o instanceof TipoAsentamiento )) return false;
        return codigo != null && codigo.equals(((TipoAsentamiento) o).getCodigo());
    }

    @Override
    public int hashCode() {
        return 900;
    }


    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<Asentamiento> getAsentamientos() {
        return asentamientos;
    }

    public void setAsentamientos(Set<Asentamiento> asentamientos) {
        this.asentamientos = asentamientos;
    }
}
