package com.sirius.normalization.model;

import javax.persistence.*;

@Entity
public class Asentamiento {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Integer codigo;
    private String nombre;
    private String nombreNormal;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "tipo_asentamiento_id")
    private TipoAsentamiento tipoAsentamiento;

    private Integer identificadorMunicipal;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "municipio_id")
    private Municipio municipio;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ciudad_id")
    private Ciudad ciudad;

    public Asentamiento() {
    }

    public Asentamiento(Integer codigo, String nombre, String nombreNormal, TipoAsentamiento tipoAsentamiento, Integer identificadorMunicipal, Municipio municipio) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.identificadorMunicipal = identificadorMunicipal;
        this.municipio = municipio;
        this.tipoAsentamiento = tipoAsentamiento;
        this.nombreNormal = nombreNormal;
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (!(o instanceof Asentamiento )) return false;
        return codigo != null && codigo.equals(((Asentamiento) o).getCodigo());
    }

    @Override
    public int hashCode() {
        return 922;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getIdentificadorMunicipal() {
        return identificadorMunicipal;
    }

    public void setIdentificadorMunicipal(Integer identificadorMunicipal) {
        this.identificadorMunicipal = identificadorMunicipal;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public TipoAsentamiento getTipoAsentamiento() {
        return tipoAsentamiento;
    }

    public void setTipoAsentamiento(TipoAsentamiento tipoAsentamiento) {
        this.tipoAsentamiento = tipoAsentamiento;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreNormal() {
        return nombreNormal;
    }

    public void setNombreNormal(String nombreNormal) {
        this.nombreNormal = nombreNormal;
    }
}
