package com.sirius.normalization.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Estado {
    @Id
    private Integer codigo;
    private String nombre;
    private String nombreNormal;

    @OneToMany(
            mappedBy = "estado",
            cascade = CascadeType.ALL,
            fetch = FetchType.LAZY
    )
    private Set<Municipio> municipios = new HashSet<>();

    public Estado(){}

    public Estado(Integer codigo, String nombre, String nombreNormal){
        this.codigo = codigo;
        this.nombre = nombre;
        this.nombreNormal = nombreNormal;
    }
    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (!(o instanceof Estado )) return false;
        return codigo != null && codigo.equals(((Estado) o).getCodigo());
    }

    @Override
    public int hashCode() {
        return 930;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Set<Municipio> getMunicipios() {
        return municipios;
    }

    public void setMunicipios(Set<Municipio> municipios) {
        this.municipios = municipios;
    }

    public String getNombreNormal() {
        return nombreNormal;
    }

    public void setNombreNormal(String nombreNormal) {
        this.nombreNormal = nombreNormal;
    }
}
